from flask import (Blueprint, request, abort)
from app.db import get_db, faz_commit
from werkzeug.security import check_password_hash, generate_password_hash
from app.classes.usuario import Usuario
import json

bp = Blueprint('api_auth', __name__)

@bp.route('/api/usuario', methods=["POST"])
def getUser():
    usuario = request.json['username']
    password = request.json['password']

    db = get_db()
    u = db.execute('SELECT * FROM usuario WHERE username=?;',(usuario,)).fetchone()

    if u is None:
        abort(404, "Usuario não encontrado")
    elif not check_password_hash(u['pass'], password):
        abort(404, "Usuario ou senha não encontrado!")
    else:
        return "{'message': '"+str(u["id"])+"'}"

@bp.route('/api/usuario/<int:id>/alterar', methods=["PUT"])
def setUser(id):
    usuario = request.json['username']
    senha = request.json['password']
    db = get_db()

    if not usuario:
        return "{'message':'Não podemos registrar sem um nome de usuario!'}"
    elif not senha:
        return "{'message': 'Não podemos registrar sem uma senha!'}"
    else:
        db.execute('UPDATE usuario SET username=?,pass=? WHERE id=?',(usuario, generate_password_hash(senha), id))
        faz_commit()
    return "{'message':'Usuario e senha alterados!'}"

@bp.route('/api/usuario/register', methods=['POST'])
def registerUser():
    usuario = request.json['username']
    senha = request.json['password']
    db = get_db()

    if not usuario:
        return "{'message':'Não podemos registrar sem um nome de usuario!'}"
    elif not senha:
        return "{'message': 'Não podemos registrar sem uma senha!'}"
    else:
        db.execute('INSERT INTO usuario (username, pass) VALUES (?,?);',(usuario, generate_password_hash(senha)))
        faz_commit()
    return "{'message':'Usuario e senha registrados no sistema!'}"
