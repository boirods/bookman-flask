from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from app.auth import login_required
from app.db import get_db, faz_commit

bp = Blueprint('capitulos', __name__)

@bp.route('/livro/<int:id>/capitulos')
@bp.endpoint('capitulos_livro')
@login_required
def capitulos_livro(id):
    db = get_db()
    capitulos_livro = db.execute(
        'SELECT c.id, c.idLivro, c.pagina, c.cap_titulo, c.idUsuario, l.id, l.titulo FROM capitulo c JOIN livro l ON l.idUsuario=c.idUsuario WHERE c.idLivro=? AND l.id=?;', (str(id), str(id))
    ).fetchall()
    if capitulos_livro != []:
        return render_template('capitulo/lista.html', capitulos_livro=capitulos_livro)
    else:
        return render_template('capitulo/sem_lista.html', id=id)

@bp.route('/livro/<int:id>/capitulo/add',methods=['GET','POST'])
@bp.endpoint('capitulos_livro_criar')
@login_required
def capitulos_livro_criar(id):
    if request.method == 'POST':
        pagina = request.form['pagina']
        cap_titulo = request.form['captitulo']
        error = None
        if not pagina:
            error = 'Numero da página de inicio do capitulo é necessária!'
        if not cap_titulo:
            error = 'Título do capítulo é necessária!'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO capitulo (idLivro, pagina, cap_titulo,idUsuario) VALUES (?,?,?,?);',
                (id, pagina, cap_titulo, g.user['id'])
            )
            db.commit()
            return redirect(url_for('capitulos.capitulos_livro', id=str(id)))
    
    return render_template('capitulo/capitulo_create.html')

def getCapitulo(id, check_user=True):
    capitulo = get_db().execute(
        'SELECT c.id, c.idLivro, c.pagina, c.cap_titulo, c.idUsuario, l.id, l.idUsuario FROM capitulo c JOIN livro l ON c.idLivro=l.id WHERE c.id=?',
        (str(id),)
    ).fetchone()
    if capitulo is None:
        abort(404, 'Capítulo id {id} não foi encontrado!')
    if check_user and capitulo['idUsuario'] != g.user['id']:
        abort(403)
    
    return capitulo

@bp.route('/livro/<int:idLivro>/capitulo/<int:id>/alterar', methods=('GET','POST'))
@bp.endpoint('capitulos_livro_alterar')
@login_required
def capitulos_livro_alterar(idLivro, id):
    capitulo = getCapitulo(id)
    if request.method == 'POST':
        #dados para atualizar um capitulo são a página e o titulo
        cap_titulo = request.form['captitulo']
        pagina = request.form['pagina']
        error = None
        
        if not cap_titulo:
            error = 'O título do capítulo é necessário!'
        if not pagina:
            error = 'O numero da página de inicio do capitulo é necessária!'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE capitulo SET pagina=?, cap_titulo=? WHERE idLivro=? AND id=?;',
                (pagina,cap_titulo,idLivro,id)
            )
            db.commit()
            return redirect(url_for('capitulos.capitulos_livro', id=str(idLivro)))
    return render_template('capitulo/capitulo_update.html', capitulo=capitulo)

@bp.route('/livro/<int:idLivro>/capitulo/<int:id>/apagar', methods=['POST'])
@login_required
def delete(idLivro, id):
    getCapitulo(id)
    db = get_db()
    db.execute('DELETE FROM capitulo WHERE id=? AND idLivro=?', (str(id), idLivro))
    db.commit()
    return redirect(url_for('biblioteca.index'))