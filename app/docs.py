from flask import Blueprint, send_from_directory
import os

docs_bp = Blueprint('docs', __name__, static_folder='docs')

@docs_bp.route('/<path:path>')
def send_docs(path):
    return send_from_directory(os.path.join(os.getcwd(), 'docs'), path)
