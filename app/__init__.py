import os
from flask import Flask, Blueprint
from flask_cors import CORS
import pytz
def create_app(test_config = None):
    app = Flask(__name__, instance_relative_config=True)
    CORS(app)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'livros.sqlite'),
        TIMEZONE=pytz.timezone('AMERICA/Sao_Paulo')
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)
    
    try:
        os.makedirs(app.instance_path)
    except:
        pass

    @app.route('/ola')
    def ola():
        return '<h1>Ola mundo, este é para ser o meu app de livros</h1>'
    from . import db
    db.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)
    
    from . import biblioteca
    app.register_blueprint(biblioteca.bp)
    app.add_url_rule('/', endpoint='index')

    from . import api_biblioteca
    app.register_blueprint(api_biblioteca.bp)

    from . import capitulo
    app.register_blueprint(capitulo.bp)
    app.add_url_rule('/capitulo', endpoint='seleciona_livro')
    
    from . import notas
    app.register_blueprint(notas.bp)
    app.add_url_rule('/notas', endpoint='seleciona_capitulo')

    from . import link
    app.register_blueprint(link.bp)
    app.add_url_rule('/links', endpoint='listar_links')

    from . import api_capitulos
    app.register_blueprint(api_capitulos.bp)

    from . import api_notas
    app.register_blueprint(api_notas.bp)

    from . import api_links
    app.register_blueprint(api_links.bp)

    from . import api_auth
    app.register_blueprint(api_auth.bp)
    
    from .docs import docs_bp
    app.register_blueprint(docs_bp, url_prefix='/docs')

    if __name__ == '__main__':
        app.run(host='0.0.0.0',port=5000)

    return app