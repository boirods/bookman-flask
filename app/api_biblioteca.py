from flask import (Blueprint, request)
from app.db import get_db, faz_commit
from app.classes.livro import Livro
import json

bp = Blueprint('api_biblioteca', __name__)

@bp.route('/api/livros')
def getAll():
    db = get_db()
    livros = db.execute(
        'SELECT * FROM livro;'
    ).fetchall()
    relivros = []
    for livro in livros:
        l = Livro(
            livro['id'],
            livro['titulo'],
            livro['edicao'],
            livro['editora'],
            livro['autores'],
            livro['idusuario']
        )
        relivros.append(l)
    return json.dumps(relivros, default=vars)

@bp.route('/api/livro/<int:id>', methods=['GET'])
def get_one(id):
    db = get_db()
    dbook = db.execute(
        'select * from livro where id=?',
        (str(id))
    ).fetchone()
    livro=Livro(dbook['id'], dbook['titulo'], dbook['edicao'], dbook['editora'], dbook['autores'], dbook['idusuario'])
    return livro.getjson()

@bp.route('/api/livros/usuario/<int:id>', methods=['GET'])
def get_all_of_user(id):
    db = get_db()
    dbooks = db.execute(
        'SELECT * FROM livro WHERE idusuario=?',
        (str(id))
    ).fetchall()
    relivros = []
    for livro in dbooks:
        l = Livro(
            livro['id'],
            livro['titulo'], 
            livro['edicao'],
            livro['editora'],
            livro['autores'],
            livro['idusuario']
        )
        relivros.append(l)
    return json.dumps(relivros, default=vars)

@bp.route('/api/usuario/<int:id>/livro/add', methods=['POST'])
def user_add_book(id):
    l = Livro(
        0,#id do livro, mas como vou registrar ainda não o sei, por isso o 0
        request.json['titulo'],
        request.json['edicao'],
        request.json['editora'],
        request.json['autores'],
        id
    )
    if l.titulo is not None:
        db = get_db()
        db.execute(
            'INSERT INTO livro (titulo, edicao, editora, autores, idusuario) VALUES (?,?,?,?,?);',
            (l.titulo, l.edicao, l.editora, l.autores, str(l.idusuario))
        )
        faz_commit()
    else:
        return {'message':'Não realizado por falta de um título'}
    return json.dumps({'message':'Dado incluido no banco com sucesso!'})

@bp.route('/api/usuario/<int:idusuario>/livro/<int:id>/alterar', methods=['PUT'])
def user_update_book(idusuario, id):
    l = Livro(
        id,
        request.json['titulo'],
        request.json['edicao'],
        request.json['editora'],
        request.json['autores'],
        idusuario
    )
    if l.titulo is not None:
        db = get_db()
        db.execute(
            'UPDATE livro SET titulo=?, edicao=?, editora=?, autores=? WHERE id=? AND idusuario=?',
            (l.titulo, l.edicao, l.editora, l.autores, id, l.idusuario)
        )
        faz_commit()
    else:
        return { 'message': 'Não podemos enviar um livro sem título' }
    return { 'message': 'Alteração realizada com sucesso'}

@bp.route('/api/usuario/<int:idusuario>/livro/<int:id>/apagar', methods=['DELETE'])
def user_delete_book(id, idusuario):
    db = get_db()
    db.execute(
        'DELETE FROM livro WHERE id=? AND idusuario=?;',
        (id, idusuario)
    )
    faz_commit()
    return { 'message':'Acreditamos que foi realizado com sucesso kkkk' }
