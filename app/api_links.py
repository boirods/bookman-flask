from flask import (Blueprint, request)
from app.db import get_db, faz_commit
from werkzeug.exceptions import abort
from app.classes.links import Link
import json

bp = Blueprint('api_links', __name__)

@bp.route('/api/links', methods=['GET'])
def getAll():
    db = get_db()
    links = db.execute(
        'SELECT * FROM link'
    ).fetchall()
    relinks = []
    for l in links:
        myLinks = Link(
            l['id'],
            l['link'],
            l['anotacao'],
            l['data_acesso'],
            l['idusuario']
        )
        relinks.append(myLinks)
    return json.dumps(relinks, default=vars)

@bp.route('/api/links/usuario/<int:id>/buscar', methods=['GET'])
def getAllUser(id):
    db = get_db()
    links = db.execute('SELECT * FROM link WHERE idusuario=?;',(str(id))).fetchall()
    relinks = []
    for l in links:
        myLinks = Link(
            l['id'],
            l['link'],
            l['anotacao'],
            l['data_acesso'],
            l['idusuario']
        )
        relinks.append(myLinks)
    return json.dumps(relinks, default=vars)

@bp.route('/api/links/usuario/<int:id>/addlink', methods=['POST'])
def addLink(id):
    if request.json['anotacao'] is not None:
        db = get_db()
        db.execute("INSERT INTO link (link, anotacao, data_acesso, idusuario) VALUES (?, ?, strftime('%d/%m/%Y %H:%M',datetime('now')),?)", (request.json['link'], request.json['anotacao'], str(id)))
        faz_commit()
    else:
        return json.dumps("{'message':'Não realizado por falta de uma anotação!'}")
    return json.dumps("{'message': 'Incluido no banco de dados com sucesso!'}")

@bp.route('/api/link/<int:id>/usuario/<int:idusuario>/alterar', methods=['PUT'])
def updateLink(id, idusuario):
    if request.json['anotacao'] is not None:
        db = get_db()
        db.execute(
            'UPDATE link SET link=?, anotacao=? WHERE id=? AND idusuario=?;', (request.json['link'], request.json['anotacao'], id, idusuario)
        )
        faz_commit()
    else:
        return json.dumps("{'message': 'Nao podemos alterar uma nota sem uma nota!'}")
    return json.dumps("{'message':'Alteracao realizada com sucesso!'}")

@bp.route('/api/link/<int:id>/usuario/<int:idusuario>/apagar', methods=['DELETE'])
def apagarLink(id, idusuario):
    db = get_db()
    if check_id_exists(id):
        dlink = db.execute('DELETE FROM link WHERE id=? AND idusuario=?;', (id, idusuario))
        faz_commit()
        if not check_id_exists(id):
            return json.dumps("{'message': 'Nota apagada com sucesso!'}")
        else:
            return json.dumps("{'message': 'Algo aconteceu e não pude apagar o link!'}")
    else:
        return json.dumps("{'message': 'Não encontramos o id informado!'}")

def check_id_exists(id):
    db = get_db()
    dlink = db.execute('SELECT * FROM link WHERE id=?;', (str(id))).fetchone()

    if dlink is not None:
        return True
    return False