from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from app.auth import login_required
from app.db import get_db

bp = Blueprint('link', __name__)

@bp.route('/link/lista')
@bp.endpoint('listar_links')
@login_required
def listar_links():
    db = get_db()
    links = db.execute(
        'SELECT id, link, anotacao, data_acesso, idusuario FROM link WHERE idusuario=?;',
        (str(g.user['id']))
    ).fetchall()
    if links != []:
        return render_template('links/index.html', links = links)
    else:
        return render_template('links/sem_lista.html')

@bp.route('/link/novo', methods=['GET', 'POST'])
@bp.endpoint('link_criar')
@login_required
def link_criar():
    if request.method == 'POST':
        flink = request.form['l']
        anotacao = request.form['nota']
        error = None
        if not flink:
            error = 'Para registrar isso precisamos de um link'
        if not anotacao:
            error = 'Para registrar isso precisamos de uma anotação'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO link (link, anotacao, data_acesso, idusuario) VALUES (?, ?, strftime('%d/%m/%Y %H:%M',datetime('now')),?)",
                (flink, anotacao, g.user['id'])
            )
            db.commit()
            return redirect(url_for('link.listar_links'))
    return render_template('links/link_create.html')

def get_link(id, check_user=True):
    link = get_db().execute(
        'SELECT id, link, anotacao, idusuario FROM link WHERE id=? AND idusuario=?;',
        (id, g.user['id'])
    ).fetchone()
    if link is None:
        abort(404, 'Link de id {id} não foi encontrado!')
    if check_user and link['idusuario'] != g.user['id']:
        abort(403)
    
    return link

@bp.route('/link/<int:id>/alterar', methods=('GET', 'POST'))
@bp.endpoint('link_alterar')
@login_required
def link_alterar(id):
    link = get_link(id)
    if request.method == 'POST':
        anotacao = request.form['anotacao']
        error=None

        if not anotacao:
            error = 'Não podemos registrar com o campo da anotação vazio!'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE link SET anotacao=? WHERE id=? and idusuario=?;',
                (anotacao, id, g.user['id'])
            )
            db.commit()
            return redirect(url_for('link.listar_links'))
    return render_template('links/update.html', link=link)

@bp.route('/link/<int:id>/apagar', methods=['POST'])
@login_required
def delete(id):
    get_link(id)
    db = get_db()
    db.execute(
        'DELETE FROM link WHERE id=? AND idusuario=?;',
        (str(id), str(g.user['id']))
    )
    db.commit()
    return redirect(url_for('link.listar_links'))
