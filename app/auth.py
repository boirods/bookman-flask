import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)

from werkzeug.security import check_password_hash, generate_password_hash

from app.db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        passw = request.form['passw']
        db = get_db()
        error = None

        if not username:
            error = 'Username é necessário.'
        elif not passw:
            error = 'Senha é necessária.'
        if error is None:
            try:
                db.execute(
                    'INSERT INTO usuario(username, pass) VALUES (?,?)',
                    (username, generate_password_hash(passw))
                )
                db.commit()
            except db.IntegrityError:
                error = f'O usuário {username} já está registrado neste sistema.'
            else:
                return redirect(url_for('auth.login'))
        flash(error)
    return render_template('auth/register.html')

@bp.route('/login',methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        passw = request.form['passw']
        db = get_db()
        error = None
        user = db.execute('SELECT * FROM usuario WHERE username=?',
            (username,)
        ).fetchone()

        if user is None:
            error = 'Usuário não encontrado em nossa base.'
        elif not check_password_hash(user['pass'], passw):
            error = 'Senha incorreta... tente novamente... quantas vezes que quiser... não bloqueará seu usuário,  e se não conseguir é porque perdeu a senha e já eras, não há recuperação. Como sugestão você pode criar outro usuário...'
        
        if error is None:
            session.clear()
            session['user_id'] = user['id']
            return redirect(url_for('index'))
        flash(error)
    return render_template('auth/login.html')

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')
    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute('SELECT * FROM usuario WHERE id=?', (str(user_id))).fetchone()

@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        
        return view(**kwargs)
    return wrapped_view
