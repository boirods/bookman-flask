from flask import (Blueprint, request)
from app.db import get_db, faz_commit
from werkzeug.exceptions import abort
from app.classes.nota import Nota
import json

bp = Blueprint('api_notas', __name__)

@bp.route('/api/notas', methods=['GET'])
def getAll():
    db = get_db()
    notas = db.execute(
        'SELECT * FROM notas;'
    ).fetchall()
    renota = []
    for n in notas:
        myNote = Nota(
            n['id'],
            n['idcapitulo'],
            n['not_pagina'],
            n['nota'],
            n['idusuario']
        )
        renota.append(myNote)
    return json.dumps(renota, default=vars)

@bp.route('/api/capitulo/<int:id>/notas', methods=['GET'])
def getAllNotesFromChapter(id):
    db = get_db()
    notas = db.execute(
        'SELECT * FROM notas WHERE idcapitulo=?',
        (str(id))
    ).fetchall()
    renota = []
    for n in notas:
        myNote = Nota(
            n['id'],
            n['idcapitulo'],
            n['not_pagina'],
            n['nota'],
            n['idusuario']
        )
        renota.append(myNote)
    return json.dumps(renota, default=vars)

@bp.route('/api/capitulo/<int:id>/addnota', methods=['POST'])
def addNotes(id):
    n = Nota(
        0,
        id,
        request.json['not_pagina'],
        request.json['nota'],
        request.json['idusuario']
    )

    if n.nota is not None:
        db = get_db()
        db.execute(
            'INSERT INTO notas (idcapitulo, not_pagina, nota, idusuario) VALUES (?,?,?,?);',
            (str(n.idCapitulo), n.pagina, n.nota, n.idusuario)
        )
        faz_commit()
    else:
        return json.dumps({ 'message': 'Não realizada por falta de uma anotação!' })
    return json.dumps({ 'message': 'Incluido com sucesso no banco de dados!' })

@bp.route('/api/capitulo/<int:idcapitulo>/nota/<int:id>/alterar', methods=['PUT'])
def setNote(idcapitulo, id):
    n = Nota(
        id,
        idcapitulo,
        request.json['pagina'],
        request.json['nota'],0
    )
    if n.nota is not None:
        db = get_db()
        db.execute(
            'UPDATE notas SET not_pagina=?, nota=? WHERE id=? AND idcapitulo=?;',
            (n.pagina, n.nota, str(n.id), str(n.idCapitulo))
        )
        faz_commit()
    else:
        return { 'message': 'Não podemos enviar uma anotação sem uma anotação!' }
    return {'message': 'Alteração realizada com suceso!'}

@bp.route('/api/capitulo/<int:idcapitulo>/nota/<int:id>/apagar', methods=['DELETE'])
def apagaNota(idcapitulo, id):
    db = get_db()
    if check_id_exists(id):
        db.execute(
            'DELETE FROM notas WHERE id=? AND idcapitulo=?;',
            (id, idcapitulo)
        )
        faz_commit()
        if not check_id_exists(id):
            return json.dumps('{ "message": "Nota apagada com sucesso!"')
        else:
            return json.dumps('{ "message": Algum problema ocorreu, e a nota não foi apagada!" }')
    else:
        return json.dumps('{ "message": "Não encontramos o id '+id+'!" }')
    
        

def check_id_exists(id):
    db = get_db()
    dnot = db.execute('SELECT * FROM notas WHERE id=?',
        (str(id))
    ).fetchone()

    if dnot is not None:
        return True
    return False
