import json

class Livro:
    def __init__(self, id, titulo, edicao, editora, autores, idusuario):
        self.id = id
        self.titulo = titulo
        self.edicao = edicao
        self.editora = editora
        self.autores = autores
        self.idusuario = idusuario

    def getjson(self):
        return json.dumps(
            {
                'id': self.id,
                'titulo': self.titulo,
                'edicao': self.edicao,
                'editora': self.editora, 
                'autores': self.autores,
                'idusuario': self.idusuario
            }
        )
