import json

class Link:
    def __init__(self, id, link, anotacao, data_acesso, idusuario):
        self.id = id
        self.link = link
        self.anotacao = anotacao
        self.data_acesso = data_acesso
        self.idusuario = idusuario

    def getjson(self):
        return json.dumps("{'id': self.id,'link': self.link,'anotacao': self.anotacao,'data_acesso': self.data_acesso,'idusuario': self.idusuario}")
