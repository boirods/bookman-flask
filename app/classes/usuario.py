import json
class Usuario:
    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password

    def getjson(self):
        return json.dumps(
            {
                "id":self.id,
                "username": self.username,
                "password": self.password
            }
        )