import json

class Nota:
    def __init__(self, id, idCapitulo, pagina, nota, idusuario):
        self.id = id
        self.idCapitulo = idCapitulo
        self.pagina = pagina
        self.nota = nota
        self.idusuario = idusuario

    def getjson(self):
        return json.dumps(
            {
                'id': self.id,
                'titulo': self.titulo,
                'edicao': self.edicao,
                'editora': self.editora, 
                'autores': self.autores,
                'idusuario': self.idusuario
            }
        )
